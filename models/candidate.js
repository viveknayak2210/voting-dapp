var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/logapp');
// Candiate Schema
var CandidateSchema = mongoose.Schema({
	name: {
		type: String,
		index:true
	}
});

var Candidate = module.exports = mongoose.model('Candidate', CandidateSchema);

module.exports.createCandidate = function(newCandidate, callback){
	newCandidate.save(callback);
}

module.exports.getCandidateDatabase= function(callback){
	Candidate.find({},callback);
}