var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/logapp');
// Voter Schema
var VoterSchema = mongoose.Schema({
	hash: {
		type: String
	}
});

var Voter = module.exports = mongoose.model('Voter', VoterSchema);

module.exports.createVoter = function(newVoter, callback){
	newVoter.save(callback);
}

module.exports.getVoterByHash = function(hash, callback){
	var query = {hash: hash};
	Voter.findOne(query, callback);
}

module.exports.getHashDatabase= function(callback){
	Voter.find({},callback);
}