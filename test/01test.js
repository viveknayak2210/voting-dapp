chai = require('chai');
chai.use(require('chai-as-promised'))
var DV = artifacts.require('Election');
expect = chai.expect;
should = chai.should;
assert = chai.assert;

contract("Distributed Voting Version 1", function (accounts) {
    //************Test control variables*************
    t = [0, 2, 2, 4, 4, 6, 6]; //Time array
    /*Cand_end_delay = 130; //Delay required to reach BTD
    Tok_end_delay = 130; //Delay required to reach BVC
    Vote_end_delay = 60; //Delay required to reach BVT*/
    W_num = 15; //Number of wardens.
		W_max = 15; //Maximum wardens
    C_num = 10; //Number of candidates.
    C_max = 10; //Maximum candidates
    V_num = 51; //Number of voters. 
    deposit = 20; //Minimum deposit required
    reward = 25;

    //Gas cost measurements
    Gas_max = new Array();
    Gas_min = new Array();
    Gas_avg = new Array();
    Gas_funcName = new Array();
    Gas_timesCalled = new Array();
    gas_sum = 0.0;
    gas_min = 0;
    gas_max = 0;
    gas_times = 0;
    gas_sum_1 = 0.0;
    gas_min_1 = 0;
    gas_max_1 = 0;
    gas_times_1 = 0;

    //Warden arrays
    w_addr = new Array(); //List of warden addresses
    w_ekey = new Array(); //List of warden encryption keys
    w_dkey = new Array(); //List of warden decryption keys
    w_dep = new Array(); //List of warden deposit amounts

    for (j = 0; j < W_num; j++) {
        w_addr.push(accounts[j + 1]);
        w_ekey.push('e' + (j + 1));
        w_dkey.push('d' + (j + 1));
        w_dep.push(deposit + 1 + 2 * j);
    } //Last possible warden account index is 15

    //Candidate arrays
    c_addr = new Array(); //List of candidate addresses
    c_symb = new Array(); //List of candidate symbols

    for (j = 0; j < C_num; j++) {
        c_addr.push(accounts[j + 1 + W_max]);
        c_symb.push('woke' + (j + 1));
    } //Last possible candidate account index is 21

    //Voter arrays
    v_addr = new Array(); //List of voter addresses
    v_token = new Array(); //List of voter tokens
    v_ek = new Array(); //List of ek assigned to voters
    v_index = new Array(); //List of indices assigned to voters
    v_ev = new Array(); //List of encrypted votes for voters
    v_voted = new Array(); //List of candidate numbers each voter will vote for

    for (j = 0; j < V_num; j++) {
        v_voted.push(C_num - (j % C_num)); //Manipulate this to achieve different voting tallies
        v_addr.push(accounts[j + 2 + C_max + W_max]);
        v_ev.push('voter ' + (j + 1) + ' has voted for candidate woke ' + v_voted[j]);
    }
    owner = accounts[0]; //President
    s = "(" + t[0] + ", " + t[1] + ", " + t[2] + ", " + t[3] + ", " + t[4] + ", " + t[5] + ", " + t[6] + ")"; //Cosmetics
    let contract; //Contract object

    describe('Contract Setup', function () {
        it('Create DV1 contract', function () {
            return DV.new(w_addr, deposit, reward).then(function (instance) {
                contract = instance;
            });
        });
        it('Set times (bcr,ecr,btd,etc,bvc,evc,bvt): ' + s, function () {
            return contract.storeTimes(t[0], t[1], t[2], t[3], t[4], t[5], t[6]).then(function (r) {
                if (gas_min == 0)
                    gas_min = r['receipt']['gasUsed'];
                if (r['receipt']['gasUsed'] > gas_max)
                    gas_max = r['receipt']['gasUsed'];
                else if (r['receipt']['gasUsed'] < gas_min)
                    gas_min = r['receipt']['gasUsed'];
                gas_sum += r['receipt']['gasUsed'];
                gas_times++;
                Gas_avg.push(gas_sum / gas_times);
                Gas_min.push(gas_min);
                Gas_max.push(gas_max);
                Gas_funcName.push('storeTimes');
                Gas_timesCalled.push(gas_times);
                gas_sum = 0.0;
                gas_min = 0;
                gas_max = 0;
                gas_times = 0;
            });
        });
        it('Set fake time to post BCR (' + (t[0] + 1) + ')', function () {
            return contract.setFakeNow(t[0] + 1).then(function (res) {});
        });
    });

    describe('Candidate Registration', function () {
        //Loop over candidates and register them.
        for (var index = 0; index < C_num; index++)(function (i) {
            it('Candidate ' + (i + 1) + ' registered', function () {
                return contract.registerAsCandidate(c_symb[i], {
                    'from': c_addr[i]
                }).then(function (r) {
                    if (gas_min == 0)
                        gas_min = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min = r['receipt']['gasUsed'];
                    gas_sum += r['receipt']['gasUsed'];
                    gas_times++;
                    return contract.candList(i).then(function (value) {
                        expect(value[0]).to.be.equal(c_symb[i]);
                        expect(value[1].toNumber()).to.be.equal(i);
                        expect(value[2]).to.be.equal(c_addr[i]);
                    });
                });
            });
        })(index)
        it('Gas cost calculations wrapped up', function () {
            Gas_avg.push(gas_sum / gas_times);
            Gas_min.push(gas_min);
            Gas_max.push(gas_max);
            Gas_funcName.push('registerAsCandidate');
            Gas_timesCalled.push(gas_times);
            gas_sum = 0.0;
            gas_min = 0;
            gas_max = 0;
            gas_times = 0;
        });
        it('Set fake time to post BTD (' + (t[2] + 1) + ')', function () {
            return contract.setFakeNow(t[2] + 1).then(function (res) {});
        });
    });

    describe('Token Assignment', function () {
        //Loop over voters and retrieve tokens
        for (var index = 0; index < V_num; index++)(function (i) {
            it('Voter ' + (i + 1) + ' gets token', function () {
                return contract.getToken({
                    'from': v_addr[i]
                }).then(function (r) {
                    if (gas_min == 0)
                        gas_min = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min = r['receipt']['gasUsed'];
                    gas_sum += r['receipt']['gasUsed'];
                    gas_times++;
                    c = r['logs'][0]['args']['token'];
                    return contract.isTokenPresent(r['logs'][0]['args']['token'], {
                        'from': v_addr[i]
                    }).then(function (value) {
                        v_token.push(c);
                        expect(value.toString()).to.be.equal('true');
                    });
                });
            });
        })(index)
        it('Gas cost calculations wrapped up', function () {
            Gas_avg.push(gas_sum / gas_times);
            Gas_min.push(gas_min);
            Gas_max.push(gas_max);
            Gas_funcName.push('getToken');
            Gas_timesCalled.push(gas_times);
            gas_sum = 0.0;
            gas_min = 0;
            gas_max = 0;
            gas_times = 0;
        });
    });

    describe('Warden Updates', function () {
        for (var index = 0; index < W_num; index++)(function (i) {
            it('Warden ' + (i + 1) + ' deposited amount ' + w_dep[i], function () {
                return contract.depositSecurity({
                    'from': w_addr[i],
                    'value': w_dep[i]
                }).then(function (r) {
                    if (gas_min == 0)
                        gas_min = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min = r['receipt']['gasUsed'];
                    gas_sum += r['receipt']['gasUsed'];
                    gas_times++;
                });
            });
            it('Warden ' + (i + 1) + ' submitted encryption key', function () {
                return contract.submitEncryptionKey(w_ekey[i], {
                    'from': w_addr[i]
                }).then(function (r) {
                    if (gas_min_1 == 0)
                        gas_min_1 = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max_1 = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min_1 = r['receipt']['gasUsed'];
                    gas_sum_1 += r['receipt']['gasUsed'];
                    gas_times_1++;
                });
            });
        })(index)
        it('Gas cost calculations wrapped up', function () {
            Gas_avg.push(gas_sum / gas_times);
            Gas_min.push(gas_min);
            Gas_max.push(gas_max);
            Gas_funcName.push('depositSecurity');
            Gas_timesCalled.push(gas_times);
            gas_sum = 0.0;
            gas_min = 0;
            gas_max = 0;
            gas_times = 0;
            Gas_avg.push(gas_sum_1 / gas_times_1);
            Gas_min.push(gas_min_1);
            Gas_max.push(gas_max_1);
            Gas_funcName.push('submitEncryptionKey');
            Gas_timesCalled.push(gas_times_1);
            gas_sum_1 = 0.0;
            gas_min_1 = 0;
            gas_max_1 = 0;
            gas_times_1 = 0;
        });
        it('Set fake time to post BVC (' + (t[4] + 1) + ')', function () {
            return contract.setFakeNow(t[4] + 1).then(function (res) {});
        });
    });

    describe('Voting phase', function () {
        for (var index = 0; index < V_num; index++)(function (i) {
            it('Voter ' + (i + 1) + ' gets en-key', function () {
                return contract.getEncryptionKey({
                    'from': v_addr[i]
                }).then(function (r) {
                    if (gas_min == 0)
                        gas_min = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min = r['receipt']['gasUsed'];
                    gas_sum += r['receipt']['gasUsed'];
                    gas_times++;
                    v_index.push(r['logs'][0]['args']['indexOfEDPair'].toNumber());
                    v_ek.push(r['logs'][0]['args']['ek']);
                    expect(r['logs'][0]['args']['indexOfEDPair'].toNumber()).to.be.equal(i % W_num + 1);
                    expect(r['logs'][0]['args']['ek']).to.be.equal(w_ekey[i % W_num]);
                });
            });
            it('Voter ' + (i + 1) + ' votes for candidate ' + v_voted[i], function () {
                return contract.castVote(v_token[i], v_index[i], v_ev[i], {
                    'from': v_addr[i]
                }).then(function (r) {
                    if (gas_min_1 == 0)
                        gas_min_1 = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max_1 = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min_1 = r['receipt']['gasUsed'];
                    gas_sum_1 += r['receipt']['gasUsed'];
                    gas_times_1++;
                    expect(r['logs'][0]['args']['voter']).to.be.equal(v_addr[i]);
                    expect(r['logs'][0]['args']['voterToken']).to.be.equal(v_token[i]);
                    expect(r['logs'][0]['args']['ev']).to.be.equal(v_ev[i]);
                });
            });
        })(index)
        it('Gas cost calculations wrapped up', function () {
            Gas_avg.push(gas_sum / gas_times);
            Gas_min.push(gas_min);
            Gas_max.push(gas_max);
            Gas_funcName.push('getEncryptionKey');
            Gas_timesCalled.push(gas_times);
            gas_sum = 0.0;
            gas_min = 0;
            gas_max = 0;
            gas_times = 0;
            Gas_avg.push(gas_sum_1 / gas_times_1);
            Gas_min.push(gas_min_1);
            Gas_max.push(gas_max_1);
            Gas_funcName.push('castVote');
            Gas_timesCalled.push(gas_times_1);
            gas_sum_1 = 0.0;
            gas_min_1 = 0;
            gas_max_1 = 0;
            gas_times_1 = 0;
        });
        it('Set fake time to post BVT (' + (t[6] + 1) + ')', function () {
            return contract.setFakeNow(t[6] + 1).then(function (res) {});
        });

    });

    describe('Tally Phase', function () {
        for (var index = 0; index < W_num; index++)(function (i) {
            it('Warden ' + (i + 1) + ' submits de-key', function () {
                return contract.submitDecryptionKey(w_dkey[i], {
                    'from': w_addr[i]
                }).then(function (r) {
                    if (gas_min == 0)
                        gas_min = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min = r['receipt']['gasUsed'];
                    gas_sum += r['receipt']['gasUsed'];
                    gas_times++;
                });
            });
        })(index)
        it('Gas cost calculations wrapped up', function () {
            Gas_avg.push(gas_sum / gas_times);
            Gas_min.push(gas_min);
            Gas_max.push(gas_max);
            Gas_funcName.push('submitDecryptionKey');
            Gas_timesCalled.push(gas_times);
            gas_sum = 0.0;
            gas_min = 0;
            gas_max = 0;
            gas_times = 0;
        });
        it('President tallies votes', function () {
            return contract.voteTally().then(function (r) {
                if (gas_min == 0)
                    gas_min = r['receipt']['gasUsed'];
                if (r['receipt']['gasUsed'] > gas_max)
                    gas_max = r['receipt']['gasUsed'];
                else if (r['receipt']['gasUsed'] < gas_min)
                    gas_min = r['receipt']['gasUsed'];
                gas_sum += r['receipt']['gasUsed'];
                gas_times++;
                Gas_avg.push(gas_sum / gas_times);
                Gas_min.push(gas_min);
                Gas_max.push(gas_max);
                Gas_funcName.push('voteTally');
                Gas_timesCalled.push(gas_times);
                gas_sum = 0.0;
                gas_min = 0;
                gas_max = 0;
                gas_times = 0;
            });
        });

        for (var index = 0; index < W_num; index++)(function (i) {
            it('First vote received by warden ' + (i + 1), function () {
                return contract.getTally(i + 1).then(function (r) {
                    console.log('First vote stored by warden', i + 1, ':', r.toString());
                });
            });
        })(index)

        for (var index = 0; index < W_num; index++)(function (i) {
            it('Reward for warden ' + (i + 1), function () {
                return contract.withdrawReward({'from': w_addr[i]
                }).then(function (r) {
                    if (gas_min == 0)
                        gas_min = r['receipt']['gasUsed'];
                    if (r['receipt']['gasUsed'] > gas_max)
                        gas_max = r['receipt']['gasUsed'];
                    else if (r['receipt']['gasUsed'] < gas_min)
                        gas_min = r['receipt']['gasUsed'];
                    gas_sum += r['receipt']['gasUsed'];
                    gas_times++;
                });
            });
        })(index)
        it('Wrapping up gas computations ', function () {
            Gas_avg.push(gas_sum / gas_times);
            Gas_min.push(gas_min);
            Gas_max.push(gas_max);
            Gas_funcName.push('withdrawReward');
            Gas_timesCalled.push(gas_times);
            gas_sum = 0.0;
            gas_min = 0;
            gas_max = 0;
            gas_times = 0;
        });
    });
    describe('', function () {
        it('Costs computed', function () {
            console.log('----------------GAS COSTS----------------');
            console.log('Voters:', V_num);
            console.log('Candidates:', C_num);
            console.log('Wardens:', W_num);
            console.log('Function: Minimum Average Maximum Times_called')
            for (var k = 0; k < Gas_funcName.length; k++) {
                if (Gas_timesCalled[k] == 1) {
                    console.log('(', k + 1, ') ', Gas_funcName[k], ':', Gas_avg[k]);
                    continue;
                }
                console.log('(', k + 1, ') ', Gas_funcName[k], ':', Gas_min[k], Gas_avg[k], Gas_max[k], Gas_timesCalled[k]);
            }
            console.log('-----------------------------------------');
        });
    });
});