var Warden = require('../models/warden.js');
var Ops = require('../src/js/config.js');
var DV = artifacts.require('Election');
DV.deployed().then(function (instance) {
    contract = instance;
    contract.setFakeNow(3).then(function (r) {
        Warden.getWardenDatabase(function (err, res) {
            if (err) {
                console.log(err);
            } else {
                console.log("logging wardens");
                console.log(res);
                var addresses = [];
                for (var index = 0; index < res.length; index++) {
                    (function (i) {
                        addresses.push(res[i].address);
                    })(index);
                }
                console.log("Addresses: ", addresses);
                return contract.storeWardens(addresses, {
                    'from': Ops.prez
                }).then(function (r) {
                    var Voter = require('../models/voter.js');
                    Voter.getHashDatabase(function (err, res) {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("logging hashes");
                            console.log(res);
                            for (var index = 0; index < res.length; index++)(function (i) {
                                return contract.storeVoterHash(res[i].hash, {
                                    'from': Ops.prez
                                }).then(function (r) {
                                    console.log('Hash stored: ', i)
                                })
                            })(index);
                            var Candidate = require('../models/candidate.js');
                            Candidate.getCandidateDatabase(function (err, res) {
                                var ctr=0;
                                if (err) console.log(err);
                                else {
                                    console.log("logging candidates")
                                    console.log(res);
                                    for (var index = 0; index < res.length; index++)(function (i) {
                                        return contract.registerCandidate(res[i].name, {
                                            'from': Ops.prez
                                        }).then(function (r) {
                                            ctr++;
                                            console.log('Candidate stored: ', i)
                                            if (ctr == res.length) console.log('Press Ctrl+C and then terminate batch (press Y) to continue...');
                                        });
                                    })(index);
                                }
                            });
                        }
                    });
                });
            }
        });
    });
});
module.exports = function (d) {};