var Election = artifacts.require("./Election.sol");
var Ops = require('../src/js/config.js')
module.exports = function (deployer) {
  deployer.deploy(Election, Ops.deposit, Ops.reward, Ops.bvc, Ops.evc, Ops.bvt);
};