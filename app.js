App = {
  //Provides web3 object
  web3Provider: null,
  //Options (imported from config_duplicate.js)
  ops: Opts,
  //Alphabet for encryption and decryption
  alphabet: "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ \nπ®ƒ©∆",
  //Messages for success events
  success_messages: {
    "submit_ekey": "<strong>Success! </strong>Your encryption key has been submitted!",
    "cast_vote": "<strong>Success! </strong>You have voted!",
    "submit_dkey": "<strong>Success! </strong>Your decryption key has been submitted!"
  },
  //Messages for error events
  error_messages: {
    "render": "<strong>Error: </strong> Rendering Failed. This could be because:<br>-No candidates registered for the election<br>- Block time could not be fetched",
    "increase_fake_time": "<strong>Error: </strong> Time forward failed: fakeTime could not be changed",
    "decrease_fake_time": "<strong>Error: </strong> Time revert failed: fakeTime could not be changed",
    "submit_ekey": "<strong>Error: </strong> Key submission failed. This could be because:<br>-You are not a registered warden<br>-You have already deposited security amount<br>-Your deposit is below the minimum required<br>-You ran out of gas",
    "get_ekey": "<strong>Error: </strong> Key could not be fetched. This could be because:<br>-No keys are available",
    "cast_vote": "<strong>Error:</strong> Vote was not recorded. This could be because:<br>-Your token is invalid or already used up<br>-You ran out of gas",
    "submit_dkey": "<strong>Error: </strong> Key submission failed. This could be because:<br>-You are not a registered warden<br>-You have not deposited security amount<br>-You already submitted a decryption key<br>-Your encryption and decryption keys do not match<br>-You ran out of gas"
  },
  //Current Metamask account being used while viewing
  account: '0x0',
  //Metamask object stores current address on button clicks. Used to verify changes in address.
  metamask: null,
  //Truffle contract object
  election: null,
  //Instance can be used to call contract functions
  instance: null,
  //List of candidates
  candidates: [],
  //Counter for votes
  voteCount: [],
  //Checks if tally results have been loaded in window
  resultsLoaded: false,
  //Current encryption key being used to encrypt vote. Format: "Generator,Post-exponentiation-value"
  receivedEkey: null,
  //Current key pair index being used to encrypt vote
  receivedIndex: null,
  //Modular exponentiation function. Returns base^exp(mod)
  modPow: function (base, exp, mod) {
    var c, x;
    if (exp === 0) {
      return 1;
    } else if (exp < 0) {
      exp = -exp;
      base = modInv(base, mod);
    }
    c = 1;
    while (exp > 0) {
      if (exp % 2 === 0) {
        base = (base * base) % mod;
        exp /= 2;
      } else {
        c = (c * base) % mod;
        exp--;
      }
    }
    return c;
  },
  //Encrypts votes. alpha=range of symbols, gen=Generator, B=Post-exponentation.
  Encryptor: function (alpha, gen, B, Prime) {
    var p, B, encrypt, f, modInv, modPow, toAlpha;
    p = Prime;
    toAlpha = function (x) {
      var y, p, l, n;
      if (x === 0) {
        return "!!!!";
      }
      y = [];
      n = 4;
      n = Math.ceil(n);
      while (n--) {
        p = Math.pow(alpha.length, n);
        l = Math.floor(x / p);
        y.push(alpha[l]);
        x -= l * p;
      }
      y = y.join("");
      return y;
    };
    modInv = function (gen, mod) {
      var v, d, u, t, c, q;
      v = 1;
      d = gen;
      t = 1;
      c = mod % gen;
      u = Math.floor(mod / gen);
      while (d > 1) {
        q = Math.floor(d / c);
        d = d % c;
        v = v + q * u;
        if (d) {
          q = Math.floor(c / d);
          c = c % d;
          u = u + q * v;
        }
      }
      return d ? v : mod - u;
    };
    modPow = function (base, exp, mod) {
      var c, x;
      if (exp === 0) {
        return 1;
      } else if (exp < 0) {
        exp = -exp;
        base = modInv(base, mod);
      }
      c = 1;
      while (exp > 0) {
        if (exp % 2 === 0) {
          base = (base * base) % mod;
          exp /= 2;
        } else {
          c = (c * base) % mod;
          exp--;
        }
      }
      return c;
    };
    encrypt = function (key, d) {
      var k, a;
      k = Math.ceil(Math.sqrt(Math.random() * Math.random()) * 1E10);
      d = alpha.indexOf(d) + 2;
      a = [];
      a[0] = modPow(key[1], k, key[0]);
      a[1] = (d * modPow(key[2], k, key[0])) % key[0];
      return a;
    };
    f = function (message, key) {
      var n, x, y, w;
      y = [];
      message = message.split("");
      n = message.length;
      while (n--) {
        x = encrypt(key, message[n]);
        y.push(toAlpha(x[0]));
        y.push(toAlpha(x[1]));
      }
      y = y.join("");
      return y;
    };
    return {
      pubKey: [p, gen, B],
      encrypt: f
    };
  },
  //Decrypts votes. alpha=range of symbols, C=Exponent
  Decryptor: function (alpha, C) {
    var p, decrypt, g, modInv, modPow, to10;
    to10 = function (x) {
      var y, p, n;
      y = 0;
      p = 1;
      x = x.split("");
      n = x.length;
      while (n--) {
        y += alpha.indexOf(x[n]) * p;
        p *= alpha.length;
      }
      return y;
    };
    modInv = function (gen, mod) {
      var v, d, u, t, c, q;
      v = 1;
      d = gen;
      t = 1;
      c = mod % gen;
      u = Math.floor(mod / gen);
      while (d > 1) {
        q = Math.floor(d / c);
        d = d % c;
        v = v + q * u;
        if (d) {
          q = Math.floor(c / d);
          c = c % d;
          u = u + q * v;
        }
      }
      return d ? v : mod - u;
    };
    modPow = function (base, exp, mod) {
      var c, x;
      if (exp === 0) {
        return 1;
      } else if (exp < 0) {
        exp = -exp;
        base = modInv(base, mod);
      }
      c = 1;
      while (exp > 0) {
        if (exp % 2 === 0) {
          base = (base * base) % mod;
          exp /= 2;
        } else {
          c = (c * base) % mod;
          exp--;
        }
      }
      return c;
    };
    p = 91744613;
    C = parseInt(C, 10);
    decrypt = function (a) {
      var d, x, y;
      x = a[1];
      y = modPow(a[0], -C, p);
      d = (x * y) % p;
      d = Math.round(d) % p;
      return alpha[d - 2];
    };
    g = function (message) {
      var n, m, d, x;
      m = [];
      n = message.length / 8;
      while (n--) {
        x = message[8 * n + 4];
        x += message[8 * n + 5];
        x += message[8 * n + 6];
        x += message[8 * n + 7];
        m.unshift(x);
        x = message[8 * n];
        x += message[8 * n + 1];
        x += message[8 * n + 2];
        x += message[8 * n + 3];
        m.unshift(x);
      }
      x = [];
      d = [];
      n = m.length / 2;
      while (n--) {
        x[0] = m[2 * n];
        x[1] = m[2 * n + 1];
        x[0] = to10(x[0]);
        x[1] = to10(x[1]);
        d.push(decrypt(x));
      }
      message = d.join("");
      return message;
    };
    return {
      priKey: C,
      decrypt: g
    };
  },
  //Starts web app
  init: function () {
    return App.initWeb3();
  },
  //Retrieves provider and sets web3 object
  initWeb3: function () {
    App.metamask = web3.currentProvider;
    App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    web3 = new Web3(App.web3Provider);
    return App.initContract();
  },
  //Retrieves ABI of the contract
  initContract: function () {
    $.getJSON("Election.json", function (election) {
      App.election = TruffleContract(election);
      App.election.setProvider(App.web3Provider);
      return App.loadCandidates();
    });
  },
  //Loads candidates into the interface
  loadCandidates: function () {
    var ctr = 0; //Counts number of candidates loaded
    App.election.deployed().then(function (instance) {
      App.instance = instance; //Retrieve an instance
      return App.instance.getCandListLength().then(function (arrayLength) {
        for (var index = 0; index < arrayLength.toNumber(); index++)(function (i) {
          return App.instance.candList(i).then(function (candidate) {
            App.candidates.push(candidate[0]);
            App.voteCount.push(0);
            ctr++;
            if (ctr == arrayLength.toNumber()) { //End condition- Every candidate accounted for 
              App.listenForEvents();
              return App.render(null, null);
            }
          });
        })(index);
      });
    });
  },
  //Listen for events
  listenForEvents: function () {
    App.instance.encryptedKeyRetrieved({}, {
      fromBlock: 0,
      toBlock: 'latest'
    }).watch(function (error, event) {
      if (error) console.log(error);
      if (event['args']['voter'] == App.account) {
        $('#getKey').attr('disabled', true);
        $('#castVote').attr('disabled', false);
        App.receivedEkey = event['args']['ek'];
        App.receivedIndex = event['args']['indexOfEDPair'].toNumber();
        $('#keyDisp').html('Key: ' + App.receivedEkey);
        $('#indexDisp').html('Index: ' + App.receivedIndex);
        console.log("encryptedKeyRetrieved: ", event['args']);
      }
    });
    App.instance.voteCasted({}, {
      fromBlock: 0,
      toBlock: 'latest'
    }).watch(function (error, event) {
      if (error) console.log(error);
      if (event['args']['voter'] == App.account) {
        $('#getKey').attr('disabled', false);
        $('#castVote').attr('disabled', true);
        $('#keyDisp').html('Key: ');
        $('#indexDisp').html('Index: ');
        App.receivedEkey = null;
        App.receivedIndex = null;
        console.log("voteCasted: ", event['args']);
      }
    });
    App.instance.AmountDeposited({}, {
      fromBlock: 0,
      toBlock: 'latest'
    }).watch(function (error, event) {
      if (error) console.log(error);
      if (event['args']['warden'] == App.account)
        console.log("Amount Deposited: ", event['args']);
    });
    App.instance.KeySubmitted({}, {
      fromBlock: 0,
      toBlock: 'latest'
    }).watch(function (error, event) {
      if (error) console.log(error);
      if (event['args']['warden'] == App.account)
        console.log("KeySubmitted: ", event['args']);
    });
    App.instance.RewardStatus({}, {
      fromBlock: 0,
      toBlock: 'latest'
    }).watch(function (error, event) {
      if (error) console.log(error);
      if (event['args']['warden'] == App.account)
        console.log("RewardStatus: ", event['args']);
    });
  },
  //Render UI
  render: function (e_msg, s_msg) {
    $("#loader").show();
    $("#content").hide();
    var previousButton = $('#decFakeTime');
    var nextButton = $('#incFakeTime');
    var voteDiv = $('#voteDiv');
    var resultsDiv = $('#resultsDiv');
    var wardenDiv = $('#wardenDiv');
    var voteTab = $('#voteTab');
    var resultsTab = $('#resultTab');
    var wardenTab = $('#wardenTab');
    var enkeyForm = $('#enkeyForm');
    var dekeyForm = $('#dekeyForm');
    var err_alert = $('#err_display');
    var success_alert = $('#success_display');
    web3 = new Web3(App.metamask);
    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          $("#accountAddress").html("Login to Metamask to continue.");
          $('#loader').hide();
        } else {
          $("#accountAddress").html("Logged in as " + account);

          if (e_msg != null) {
            $('#error-text').html(e_msg);
            err_alert.show();
          } else {
            err_alert.hide();
            $('#error-text').html('');
          }
          if (s_msg != null) {
            $('#success-text').html(s_msg);
            success_alert.show();
          } else {
            success_alert.hide();
            $('#success-text').html('');
          }
          return App.instance.fakeNow().then(function (res) { //Get time
            //Encryption key submission time
            if (res.toNumber() < App.ops.bvc) {
              App.instance.amWarden({
                'from': App.account
              }).then(function (amWarden) {
                if (amWarden.toString() == 'false') {
                  $("#accountAddress").html("You are not a warden. Logged in as " + account);
                  $('#loader').hide();
                } else {
                  $('#currentSecurity').html("(Minimum = " + App.ops.deposit + ")");
                  if (voteTab.hasClass('active'))
                    voteTab.removeClass('active');
                  if (voteDiv.hasClass('active'))
                    voteDiv.removeClass('active');
                  if (resultsTab.hasClass('active'))
                    resultsTab.removeClass('active');
                  if (resultsDiv.hasClass('active'))
                    resultsDiv.removeClass('active');
                  if (!wardenTab.hasClass('active'))
                    wardenTab.addClass('active');
                  if (!wardenDiv.hasClass('active'))
                    wardenDiv.addClass('active');
                  voteTab.hide();
                  resultsTab.hide();
                  wardenTab.show();
                  previousButton.hide();
                  nextButton.show();
                  enkeyForm.show();
                  dekeyForm.hide();
                  $('#loader').hide();
                  $('#content').show();
                }
              });

            }
            //Voting going on 
            else if (res.toNumber() >= App.ops.bvc && res.toNumber() < App.ops.evc) {
              if (!voteTab.hasClass('active'))
                voteTab.addClass('active');
              if (!voteDiv.hasClass('active'))
                voteDiv.addClass('active');
              if (resultsTab.hasClass('active'))
                resultsTab.removeClass('active');
              if (resultsDiv.hasClass('active'))
                resultsDiv.removeClass('active');
              if (wardenTab.hasClass('active'))
                wardenTab.removeClass('active');
              if (wardenDiv.hasClass('active'))
                wardenDiv.removeClass('active');
              voteTab.show();
              resultsTab.hide();
              wardenTab.hide();
              previousButton.show();
              nextButton.show();
              enkeyForm.hide();
              dekeyForm.hide();
              $("#candSelect").empty();
              for (var index = 0; index < App.candidates.length; index++)
                $("#candSelect").append("<option value='" + index + "' >" + App.candidates[index] + "</ option>");
              $('#loader').hide();
              $('#content').show();
            }
            //Decryption key submission time
            else if (res.toNumber() >= App.ops.evc && res.toNumber() < App.ops.bvt) {
              App.instance.amWarden({
                'from': App.account
              }).then(function (amWarden) {
                if (amWarden.toString() == 'false') {
                  $("#accountAddress").html("You are not a warden. Logged in as " + account);
                  console.log("I'm here")
                  $('#loader').hide();
                } else {
                  if (voteTab.hasClass('active'))
                    voteTab.removeClass('active');
                  if (voteDiv.hasClass('active'))
                    voteDiv.removeClass('active');
                  if (resultsTab.hasClass('active'))
                    resultsTab.removeClass('active');
                  if (resultsDiv.hasClass('active'))
                    resultsDiv.removeClass('active');
                  if (!wardenTab.hasClass('active'))
                    wardenTab.addClass('active');
                  if (!wardenDiv.hasClass('active'))
                    wardenDiv.addClass('active');
                  voteTab.hide();
                  resultsTab.hide();
                  wardenTab.show();
                  previousButton.show();
                  nextButton.show();
                  enkeyForm.hide();
                  dekeyForm.show();
                  $('#loader').hide();
                  $('#content').show();
                }
              });

            }
            //Tallying has begun 
            else {
              if (voteTab.hasClass('active'))
                voteTab.removeClass('active');
              if (voteDiv.hasClass('active'))
                voteDiv.removeClass('active');
              if (!resultsTab.hasClass('active'))
                resultsTab.addClass('active');
              if (!resultsDiv.hasClass('active'))
                resultsDiv.addClass('active');
              resultsDiv.addClass('in');
              if (wardenDiv.hasClass('active'))
                wardenDiv.removeClass('active');
              if (wardenTab.hasClass('active'))
                wardenTab.removeClass('active');
              voteTab.hide();
              resultsTab.show();
              resultsDiv.show();
              wardenTab.hide();
              previousButton.show();
              nextButton.hide();
              enkeyForm.hide();
              dekeyForm.hide();
              $('#candResults').empty();
              if (App.resultsLoaded == false) {
                return App.instance.numberOfWardens().then(function (num) {
                  for (var index = 0; index < num.toNumber(); index++)(function (j) {
                    return App.instance.deKeyVerified(j + 1).then(function (isVerif) {
                      if (isVerif == true) {
                        return App.instance.inverseDecIndex(j + 1).then(function (real_index) {
                          return App.instance.deKeys(real_index.toNumber()).then(function (exponent) {
                            return App.instance.voteBatchTallyArrayLength(j + 1).then(function (res) {
                              var ctr = 0;
                              for (var index = 0; index < res.toNumber(); index++)(function (i) {
                                return App.instance.getVote(j + 1, i).then(function (r) {
                                  App.voteCount[App.Decryptor(App.alphabet, exponent).decrypt(r)]++;
                                  ctr++;
                                  if (ctr == res.toNumber()) {
                                    App.resultsLoaded = true;
                                    for (var index = 0; index < App.candidates.length; index++) {
                                      $('#candResults').append("<tr><th>" + (index + 1) + "</th><td>" + App.candidates[index] + "</td><td>" + App.voteCount[index] + "</td></tr>");
                                    }
                                    $('#loader').hide();
                                    $('#content').show();
                                  }
                                }).catch(function (err) {
                                  console.log(err);
                                });
                              })(index);
                            });
                          });
                        })
                      }
                    });
                  })(index);
                });
              } else {
                for (var index = 0; index < App.candidates.length; index++) {
                  $('#candResults').append("<tr><th>" + (index + 1) + "</th><td>" + App.candidates[index] + "</td><td>" + App.voteCount[index] + "</td></tr>");
                }
                $('#loader').hide();
                $('#content').show();
              }
            }
          }).catch(function (error) {
            console.log(error);
            //App.render(App.error_messages.render, null);
          });
        }
      }
    });
  },
  //Increase fake time
  incFakeTime: function () {
    web3 = new Web3(App.metamask);
    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          App.render(null, null);
        } else {
          $("#accountAddress").html("Logged in as " + account);
          return App.instance.fakeNow().then(function (res) {
            return App.instance.setFakeNow(res.toNumber() + 2, {
              'from': App.account
            });
          }).then(function (res) {
            App.render(null, null);
          }).catch(function (error) {
            App.render(App.error_messages.increase_fake_time, null);
          });
        }
      }
    });
  },
  //Decrease fake time
  decFakeTime: function () {
    web3 = new Web3(App.metamask);
    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          App.render(null, null);
        } else {
          $("#accountAddress").html("Logged in as " + account);
          return App.instance.fakeNow().then(function (res) {
            return App.instance.setFakeNow(res.toNumber() - 2, {
              'from': App.account
            });
          }).then(function (res) {
            App.render(null, null);
          }).catch(function (error) {
            App.render(App.error_messages.decrease_fake_time, null);
          });
        }
      }
    });
  },
  //Submit encryption key (Warden function)
  submitEnKey: function () {
    web3 = new Web3(App.metamask);
    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          App.render(null, null);
        } else {
          $("#accountAddress").html("Logged in as " + account);
          var loader = $("#loader");
          var content = $("#content");
          loader.show();
          content.hide();
          return App.instance.depositSecurity({
            'from': App.account,
            'value': $('#security').val()
          }).then(function (res) {
            return App.instance.submitEncryptionKey($('#eKeygenSubmit').val().toString() + ',' + $('#ePowerSubmit').val().toString() + ',' + $('#ePrimeSubmit').val().toString(), {
              'from': App.account,
              'gas': 1000000
            });
          }).then(function (res) {
            $("#security").val('');
            $('#eKeygenSubmit').val('');
            $('#ePowerSubmit').val('');
            $('#ePrimeSubmit').val('');
            App.render(null, App.success_messages.submit_ekey);
          }).catch(function (err) {
            $("#security").val('');
            $('#eKeygenSubmit').val('');
            $('#ePowerSubmit').val('');
            $('#ePrimeSubmit').val('');
            App.render(App.error_messages.submit_ekey, null);
          });
        }
      }
    });
  },
  //Get encryption key (Voter function)
  getKey: function () {
    web3 = new Web3(App.metamask);
    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          App.render(null, null);
        } else {
          $("#accountAddress").html("Logged in as " + account);
          return App.instance.getEncryptionKey({
            'from': App.account
          }).then(function (res) {
            App.render(null, null);
          }).catch(function (error) {
            console.log(error);
            App.render(App.error_messages.get_ekey, null);
          });
        }
      }
    });

  },
  //Cast a vote (Voter function)
  castVote: function () {
    web3 = new Web3(App.metamask);
    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          App.render(null, null);
        } else {
          $("#accountAddress").html("Logged in as " + account);
          var token = $('#tok').val();
          var chosenCandID = $('#candSelect').val();
          return App.instance.candList(chosenCandID).then(function (res) {
            var ingredients = App.receivedEkey.split(","); //Separate portions of the key
            var e = App.Encryptor(App.alphabet, ingredients[0], ingredients[1], ingredients[2]);
            return App.instance.castVote(CryptoJS.SHA256(token).toString(), //Hashing token
              App.receivedIndex, //Passing key pair index
              e.encrypt(chosenCandID, e.pubKey), //encrypt the candidate ID
              {
                'from': App.account,
                'gas': 1500000
              });
          }).then(function (res) {
            $('#tok').val('');
            App.render(null, App.success_messages.cast_vote);
          }).catch(function (err) {
            $('#tok').val('');
            console.log(err);
            App.render(App.error_messages.cast_vote, null);
          });
        }
      }
    });
  },
  //Submit decryption key (Warden function)
  submitDeKey: function () {
    web3 = new Web3(App.metamask);

    web3.eth.getCoinbase(function (err, account) {
      if (err === null && web3.currentProvider == App.metamask) {
        App.account = account;
        if (account == null) {
          App.render(null, null);
        } else {
          $("#accountAddress").html("Logged in as " + account);
          var warden_index;
          var loader = $("#loader");
          var content = $("#content");
          loader.show();
          content.hide();
          return App.instance.getWardenIndex(App.account).then(function (r) {
            if (r.toNumber() == 0)
              throw "Not a warden";
            warden_index = r.toNumber();
            return App.instance.getEKey(r.toNumber());
          }).then(function (r) {
            var ingredients = r.split(",");
            if (App.modPow(ingredients[0], $('#dkeySubmit').val(), ingredients[2]) == ingredients[1]) {
              exponent = $('#dkeySubmit').val();
              return App.instance.submitDecryptionKey($('#dkeySubmit').val(), {
                'from': App.account,
                'gas': 1250000
              });
            } else {
              throw "Incorrect Exponent";
            }
          }).then(function (res) {
            return App.instance.withdrawReward({
              'from': App.account
            });
          }).then(function (res) {
            $("#dkeySubmit").val('');
            App.render(null, App.success_messages.submit_dkey);
          }).catch(function (err) {
            console.log(err);
            $("#dkeySubmit").val('');
            App.render(App.error_messages.submit_dkey, null);
          });
        }
      }
    });
  },
};

$(function () {
  $(window).load(function () {
    App.init();
  });
});