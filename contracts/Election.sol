pragma solidity ^0.4.18;
/*SUMMARY:
On-chain methods in this implementation:

Off-chain methods in this implementation:

*/
contract Election {
    /*-------------------TESTING CODE-----------------*/
    uint public fakeNow=0; //only for testing. Replace with 'now' during deployment
    /* ------------------ Variables --------------------------- */
    struct Candidate {
        string name;
        uint id;
    }
    
    address private _president;
    
    Candidate[] public candList;
    uint private securityAmt;
    uint private reward;
    uint public numberOfWardens = 0;
    mapping(string => bool) private hashDatabase;
    mapping(address => uint) private wardens;
    mapping(uint => uint) private eIndex;
    mapping(uint => uint) private inverseIndex;
    mapping(uint => uint) private dIndex;
    mapping(uint => uint) public inverseDecIndex;
    mapping(uint => bool) private EkeyReceived;
    mapping(uint => bool) private DkeyReceived;
    mapping(uint => bool) private depositReceived;
    mapping(address => uint) private refundAmt;
    mapping(uint => string) private enKeys;
    mapping(uint => string) public deKeys;
    mapping(uint => bool) public deKeyVerified;
    mapping(uint => string[]) private voteBatch;
    mapping(uint => string[]) public voteBatchTally;
    mapping(uint => uint) public voteBatchTallyArrayLength;

    uint public bvc;
    uint public evc;
    uint public bvt;
    uint private idCounter = 0;
    uint private marker = 0;
    uint private numKeys = 0;
    
    modifier isPresident {
        require(msg.sender == _president);
        _;
    }    
    modifier moreThanOneKey {
        require(numKeys>0);
        _;
    }
    modifier isInVoteCastingTime {
        uint presentTimeStamp = fakeNow;
        require(presentTimeStamp > bvc && presentTimeStamp < evc);
        _;
    }
    modifier votingIsOver {
        require(fakeNow >= evc && fakeNow < bvt);
        _;
    }
    modifier canVoteTallyStart {
        require(fakeNow > bvt);
        _;
    }
    modifier isWarden {
        require(wardens[msg.sender] > 0);
        _;
    }
    modifier hasSubmittedEKey {
        require(EkeyReceived[wardens[msg.sender]]==true);
        _;
    }
    modifier hasNotSubmittedEKey {
        require(EkeyReceived[wardens[msg.sender]]==false);
        _;
    }
    modifier hasNotSubmittedDKey {
        require(DkeyReceived[wardens[msg.sender]]==false);
        _;
    }
    modifier hasNotDeposited {
        require(depositReceived[wardens[msg.sender]]==false);
        _;
    }
    modifier beforeVoteCastTime {
        require(fakeNow < bvc);
        _;
    }
    modifier hasMinSecurityAmount {
        require(msg.value >= securityAmt);
        _;
    }
    modifier hasDepositedSecurityAmount {
        require(refundAmt[msg.sender] > 0);
        _;
    }
    event encryptedKeyRetrieved(address voter, uint modulo, uint totalKeys, uint indexOfEDPair, string ek);
    event voteCasted(address voter, string tok, string ev);
    event AmountDeposited(address warden, uint deposit);
    event KeySubmitted(address warden,string key, uint id);
    event RewardStatus(address warden,bool isSuccess,uint money);
    constructor (uint securityAmount, uint rewardAmount, uint tbvc, uint tevc, uint tbvt) public {
        bvc = fakeNow + tbvc;
        evc = fakeNow + tevc;
        bvt = fakeNow + tbvt;
        _president = msg.sender;
        securityAmt = securityAmount;
        reward = rewardAmount;
    }
    function getCandListLength() public view returns (uint value){
        return candList.length;
    }

    /* ------------------ President Methods --------------------------- */
    function setFakeNow(uint f) public {
        fakeNow = f;
    }

    function storeWardens(address[] wardensList) public isPresident beforeVoteCastTime {
        numberOfWardens = wardensList.length;
        for(uint i = 0; i < wardensList.length; i++)
            wardens[wardensList[i]] = i + 1;
    }

    function storeVoterHash(string hashPassed) public isPresident beforeVoteCastTime {
        hashDatabase[hashPassed] = true;
    }

    function registerCandidate(string name) public isPresident beforeVoteCastTime {
        uint id = candList.length;
        candList.push(
            Candidate({
                name: name,
                id: id
            })
        );
    }

    function getWardenIndex(address warden) public view votingIsOver returns (uint i){
        return wardens[warden];
    }

    function getEKey(uint index) public view votingIsOver returns (string ek) {
        return (enKeys[inverseIndex[index]]);
    }

    /* ------------------ Voter Methods --------------------------- */
    function getEncryptionKey() public isInVoteCastingTime moreThanOneKey {
        uint i= idCounter%numKeys;
        i++;
        idCounter++;
        emit encryptedKeyRetrieved(msg.sender, idCounter,numKeys,eIndex[i], enKeys[i]);
    }

    function castVote(string tokenHash, uint index, string ev) public isInVoteCastingTime {
        require(hashDatabase[tokenHash] == true);
        hashDatabase[tokenHash] = false;
        voteBatch[index].push(ev);
        emit voteCasted(msg.sender,tokenHash,ev);
    }

    function getVote(uint wardenIndex,uint voteIndex) public canVoteTallyStart view returns (string) {
        return voteBatch[wardenIndex][voteIndex];
    }

    /* ------------------ Warden Methods --------------------------- */    
    //PRE VOTING METHODS
    function depositSecurity() public isWarden beforeVoteCastTime hasMinSecurityAmount hasNotDeposited payable {
        refundAmt[msg.sender] = msg.value - securityAmt;
        depositReceived[wardens[msg.sender]]=true;
        emit AmountDeposited(msg.sender,msg.value);
    }

    function amWarden() public view returns (bool){
        if(wardens[msg.sender]>0)
            return true;
        else
            return false;
    }
    
    function submitEncryptionKey(string ek) public isWarden beforeVoteCastTime hasDepositedSecurityAmount hasNotSubmittedEKey {
        numKeys++;
        uint id = wardens[msg.sender];
        eIndex[numKeys]= id;
        inverseIndex[id] = numKeys;
        enKeys[numKeys] = ek;
        EkeyReceived[wardens[msg.sender]]=true;
        emit KeySubmitted(msg.sender,enKeys[numKeys],eIndex[numKeys]);
    }

    function submitDecryptionKey(string dk) public isWarden votingIsOver hasDepositedSecurityAmount hasSubmittedEKey hasNotSubmittedDKey {
        marker++;
        uint id = wardens[msg.sender]; 
        dIndex[marker] = id;
        inverseDecIndex[id] = marker;
        deKeys[marker] = dk;
        DkeyReceived[wardens[msg.sender]]=true;
        deKeyVerified[id] = true;
        voteBatchTally[id] = voteBatch[id];
        voteBatchTallyArrayLength[id] = voteBatchTally[id].length;
        emit KeySubmitted(msg.sender,deKeys[marker],dIndex[marker]);
    }

    function withdrawReward() public isWarden votingIsOver returns (bool) {
        uint amount = refundAmt[msg.sender];
        refundAmt[msg.sender] = 0;
        if(amount > 0) {
            if(!msg.sender.send(amount)) {
                refundAmt[msg.sender] = amount;
                emit RewardStatus(msg.sender, false,0);
                return false;
            }
        }
        emit RewardStatus(msg.sender,true,amount);
        return true;
    }
}