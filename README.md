## Dependencies
Install these prerequisites
- NPM: https://nodejs.org
- Truffle: https://github.com/trufflesuite/truffle
- Ganache: http://truffleframework.com/ganache/
- Metamask: https://metamask.io/
- MongoDB. Set it up at port 12017
- log-app: https://gitlab.com/viveknayak2210/log-app

## Step 1. Clone the project

## Step 2. Install dependencies
$ cd votingDapp
$ npm install

## Step 3. Start Ganache
Open the Ganache GUI client that you downloaded and installed. This will start your local blockchain instance. 

## Step 4. Run log-app
Run the log-app application and enter warden/candidate/voter data.

## Step 5. Compile & Deploy Election Smart Contract
$ truffle migrate --reset
You must migrate the election smart contract each time your restart ganache.

## Step 6. Configure Metamask
- Unlock Metamask
- Connect metamask to your local Etherum blockchain provided by Ganache.
- Import an account provided by ganache.

## Step 7. Run the Front End Application
`$ npm run dev
Visit this URL in your browser: http://localhost:3000